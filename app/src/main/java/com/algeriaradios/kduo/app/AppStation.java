package com.algeriaradios.kduo.app;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by suman on 1/15/17.
 */



public class AppStation implements Serializable , Parcelable{

    @SerializedName("image_url")
    @Expose

    public String image_url;
    @SerializedName("radio_title")
    @Expose


    public String radio_title;
    @SerializedName("stream_url")
    @Expose


    public String stream_url;
    private List<AppStation> stations = null;


    public AppStation() {
    }

    public AppStation(String image_url, String radio_title, String stream_url) {
        this.image_url = image_url;
        this.radio_title = radio_title;
        this.stream_url = stream_url;
    }



    protected AppStation(Parcel in) {
        image_url = in.readString();
        radio_title = in.readString();
        stream_url = in.readString();
    }


    public static final Creator<AppStation> CREATOR = new Creator<AppStation>() {
        @Override
        public AppStation createFromParcel(Parcel in) {
            return new AppStation(in);
        }

        @Override
        public AppStation[] newArray(int size) {
            return new AppStation[size];
        }
    };

    public List<AppStation> getChannels() {
        return stations;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getRadio_title() {
        return radio_title;
    }

    public void setRadio_title(String radio_title) {
        this.radio_title = radio_title;
    }

    public String getStream_url() {
        return stream_url;
    }

    public void setStream_url(String stream_url) {
        this.stream_url = stream_url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(image_url);
        dest.writeString(radio_title);
        dest.writeString(stream_url);
        dest.writeTypedList(stations);
    }
}
